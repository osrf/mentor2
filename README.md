# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/mentor2

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-others-gh-pages/#!/osrf/mentor2

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/mentor2

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

